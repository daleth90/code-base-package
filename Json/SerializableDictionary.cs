﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    /// <summary>
    /// This class is used to be serialized by JsonUtility.
    /// It does not shown in the inspector, so it cannot be used as an editable field.
    /// </summary>
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver {
        [SerializeField]
        private List<TKey> keys = new List<TKey>();
        [SerializeField]
        private List<TValue> values = new List<TValue>();

        public void OnAfterDeserialize() {
            Clear();
            if ( keys.Count != values.Count )
                throw new Exception( string.Format( "There are {0} keys and {1} values after deserialization."
                    + "Make sure that both key and value types are serializable.", keys.Count, values.Count ) );

            for ( int i = 0; i < keys.Count; i++ ) {
                Add( keys[ i ], values[ i ] );
            }
        }

        public void OnBeforeSerialize() {
            keys.Clear();
            values.Clear();
            foreach ( KeyValuePair<TKey, TValue> pair in this ) {
                keys.Add( pair.Key );
                values.Add( pair.Value );
            }
        }
    }
}
