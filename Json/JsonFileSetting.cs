﻿using System.IO;
using UnityEngine;

namespace DeltaPosition {
    public abstract class JsonFileSetting<T> where T : JsonFileSetting<T> {
        private static string dataPath = null;

        private static T instance;

        protected JsonFileSetting() {

        }

        public static T Instance {
            get {
                if ( string.IsNullOrEmpty( dataPath ) ) {
#if !UNITY_EDITOR && ( UNITY_IOS || UNITY_ANDROID )
                    dataPath = Application.persistentDataPath;
#else
                    DirectoryInfo dirInfo = new DirectoryInfo( Application.dataPath );
                    dataPath = dirInfo.Parent.FullName;
#endif
                }

                if ( instance == null ) {
                    instance = LoadSetting();
                }

                return instance;
            }
        }

        private static T LoadSetting() {
            string filePath = Path.Combine( dataPath, string.Format( "{0}.json", typeof( T ).Name ) );

            using ( FileStream fs = new FileStream( filePath, FileMode.OpenOrCreate ) ) {
                using ( StreamReader reader = new StreamReader( fs ) ) {
                    string allText = reader.ReadToEnd();
                    if ( string.IsNullOrEmpty( allText ) ) {
                        T setting = JsonUtility.FromJson<T>( "{}" );
                        return setting;
                    }
                    else {
                        T setting = JsonUtility.FromJson<T>( allText );
                        return setting;
                    }
                }
            }
        }

        public void SaveSetting() {
            string filePath = Path.Combine( dataPath, string.Format( "{0}.json", typeof( T ).Name ) );

            using ( FileStream fs = new FileStream( filePath, FileMode.Create ) ) {
                using ( StreamWriter writer = new StreamWriter( fs ) ) {
                    string allText = JsonUtility.ToJson( instance, true );
                    writer.Write( allText );
                }
            }
        }
    }
}
