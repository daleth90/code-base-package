﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    /// <summary>
    /// This class is used to be serialized by JsonUtility.
    /// It does not shown in the inspector, so it cannot be used as an editable field.
    /// </summary>
    [Serializable]
    public class SerializableList<T> : List<T>, ISerializationCallbackReceiver {
        [SerializeField]
        private List<T> list = new List<T>();

        public void OnAfterDeserialize() {
            Clear();
            AddRange( list );
            list.Clear();
        }

        public void OnBeforeSerialize() {
            list = new List<T>( this );
        }
    }
}
