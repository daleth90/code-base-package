﻿namespace DeltaPosition {
    public interface IProgressView {
        void Show();
        void Hide();
        void SetAmount( float amount );
    }
}
