﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace DeltaPosition {
    [RequireComponent( typeof( NavMeshAgent ) )]
    public class NavMeshAvatar : MonoBehaviour {
        private NavMeshAgent agent;

        public event Action DestinationReached;

        private bool isDestinationReachedInLastFrame = false;

        private void Awake() {
            agent = GetComponent<NavMeshAgent>();
        }

        private void Update() {
            bool isDestinationReachedInThisFrame = HasReachedDestination();
            if ( !isDestinationReachedInLastFrame && isDestinationReachedInThisFrame ) {
                agent.isStopped = true;
                DestinationReached?.Invoke();
            }

            isDestinationReachedInLastFrame = isDestinationReachedInThisFrame;
        }

        public float GetRadius() {
            return agent.radius;
        }

        private bool HasReachedDestination() {
            if ( !agent.pathPending ) {
                if ( agent.remainingDistance <= agent.stoppingDistance ) {
                    if ( !agent.hasPath || agent.velocity.sqrMagnitude == 0f ) {
                        return true;
                    }
                }
            }

            return false;
        }

        public void SetDestination( Vector3 target ) {
            SetDestination( target, 0f );
        }

        public void SetDestination( Vector3 target, float stoppingDistance ) {
            isDestinationReachedInLastFrame = false;
            agent.isStopped = false;
            agent.stoppingDistance = stoppingDistance;
            agent.SetDestination( target );
        }
    }
}
