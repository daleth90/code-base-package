﻿using System;
using UnityEngine;

namespace DeltaPosition {
    public class NavMeshCharacter {
        private readonly int instanceId;
        private readonly NavMeshAvatar avatar;
        private readonly IProgressView progressView;

        private bool isInteracting = false;
        private float interactionTime;
        private float targetInteractionTime;

        private Action onDestinationReached = null;
        private Action onInteractionFinished = null;

        public NavMeshCharacter( int instanceId, NavMeshAvatar avatar, IProgressView progressView ) {
            this.instanceId = instanceId;
            this.avatar = avatar;
            this.progressView = progressView;

            avatar.DestinationReached += delegate { onDestinationReached?.Invoke(); };
        }

        public int GetInstanceId() {
            return instanceId;
        }

        public Vector3 GetPosition() {
            return avatar.transform.position;
        }

        public float GetRadius() {
            return avatar.GetRadius();
        }

        public void SetDestination( Vector3 position ) {
            avatar.SetDestination( position );
        }

        public void SetOnDestinationReached( Action action ) {
            onDestinationReached = action;
        }

        public void SetOnInteractionFinished( Action action ) {
            onInteractionFinished = action;
        }

        public void Tick( float deltaTime ) {
            if ( isInteracting ) {
                interactionTime += deltaTime;
                progressView.SetAmount( interactionTime / targetInteractionTime );
                if ( interactionTime >= targetInteractionTime ) {
                    isInteracting = false;
                    progressView.Hide();
                    onInteractionFinished?.Invoke();
                }
            }
        }

        public void StartInteraction( float time ) {
            isInteracting = true;
            progressView.Show();

            interactionTime = 0f;
            targetInteractionTime = time;
            progressView.SetAmount( 0f );
        }

        public void StopInteraction() {
            isInteracting = false;
            progressView.Hide();
        }
    }
}
