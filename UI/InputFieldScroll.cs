﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DeltaPosition.UI {
    [RequireComponent( typeof( InputField ) )]
    public class InputFieldScroll : UIBehaviour {
        [Tooltip( "The default row count in InputField, this will be ignored if a ScrollRect is assigned" )]
        [Range( 1, 50 )]
        [SerializeField]
        private int minRowCount = 1;

        [Tooltip( "ScrollRect from parent" )]
        [SerializeField]
        private ScrollRect scrollRect = null;

        private InputField inputField;
        private RectTransform rectTransform;
        private LayoutElement layoutElement;
        private HorizontalOrVerticalLayoutGroup parentLayoutGroup;
        private float canvasScaleFactor = 1;

        protected override void Awake() {
            inputField = GetComponent<InputField>();
            inputField.onValueChanged.AddListener( new UnityAction<string>( ResizeInput ) );

            rectTransform = GetComponent<RectTransform>();
            layoutElement = GetComponent<LayoutElement>();
            parentLayoutGroup = transform.parent.GetComponent<HorizontalOrVerticalLayoutGroup>();

            CanvasScaler canvasScaler = GetComponentInParent<CanvasScaler>();
            if ( canvasScaler ) {
                canvasScaleFactor = canvasScaler.scaleFactor;
            }
        }

        // Resize the RectTransform of the InputField
        private void ResizeInput() {
            ResizeInput( inputField.text );
        }

        private void ResizeInput( string text ) {
            // Current text settings
            TextGenerationSettings settings = inputField.textComponent.GetGenerationSettings( inputField.textComponent.rectTransform.rect.size );
            settings.generateOutOfBounds = false;
            settings.scaleFactor = canvasScaleFactor; // HACK: Scale factor of settings not following the global scale factor... make sure it do

            // Get text padding (min max vertical offset for size calculation)
            float vecticalOffset = inputField.placeholder.rectTransform.offsetMin.y - inputField.placeholder.rectTransform.offsetMax.y;

            // Preferred text rect height
            float preferredHeight = ( new TextGenerator().GetPreferredHeight( text, settings ) / canvasScaleFactor ) + vecticalOffset + 10f;

            // Default text rect height (Fit to scroll parent or expand to fit text)
            float minHeight;
            if ( scrollRect )
                minHeight = scrollRect.GetComponent<RectTransform>().rect.size.y;
            else
                minHeight = ( ( new TextGenerator().GetPreferredHeight( "", settings ) * minRowCount ) / canvasScaleFactor ) + vecticalOffset;

            // Current text rect height
            float currentHeight = inputField.textComponent.rectTransform.rect.height;

            // Force to resize
            if ( Mathf.Abs( currentHeight - preferredHeight ) > Mathf.Epsilon ) {
                float newHeight = Mathf.Max( preferredHeight, minHeight ); // At least min height
                if ( parentLayoutGroup && layoutElement )
                    layoutElement.preferredHeight = newHeight;
                else
                    rectTransform.sizeDelta = new Vector2( rectTransform.rect.width, newHeight );
            }

            // Scroll to bottom if just added new line
            if ( gameObject.activeInHierarchy && inputField.caretPosition == inputField.text.Length
                && inputField.text.Length > 0 && inputField.text[ inputField.text.Length - 1 ] == '\n' ) {
                StartCoroutine( ScrollToBottomCoroutine() );
            }
        }

        // Update ScrollRect position (After the layout get rebuilt)
        private IEnumerator ScrollToBottomCoroutine() {
            yield return new WaitForEndOfFrame();
            if ( scrollRect != null )
                scrollRect.verticalNormalizedPosition = 0f;
        }
    }
}
