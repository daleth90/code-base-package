﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DeltaPosition {
    public class TaskQueue : MonoBehaviour {
        private readonly List<IEnumerator> coroutines = new List<IEnumerator>();

        private Task current = null;

        private void Update() {
            if ( current != null && current.Running ) {
                return;
            }

            if ( coroutines.Count == 0 ) {
                return;
            }

            var enumarator = coroutines[ 0 ];
            coroutines.RemoveAt( 0 );
            current = new Task( enumarator );
        }

        public void Enqueue( IEnumerator coroutine ) {
            coroutines.Add( coroutine );
        }

        public bool Remove( IEnumerator coroutine ) {
            return coroutines.Remove( coroutine );
        }
    }
}
