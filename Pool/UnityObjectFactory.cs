﻿using UnityEngine;

namespace DeltaPosition {
    public abstract class UnityObjectFactory<T> where T : Object {
        public abstract T Create();
    }
}
