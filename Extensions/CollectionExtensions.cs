﻿using System;
using System.Collections.Generic;

namespace DeltaPosition {
    public static class CollectionExtensions {
        private static Random random = new Random();

        /// <summary>
        /// This method will create a new list, and does not modify the original enumerable
        /// </summary>
        public static IEnumerable<T> Shuffle<T>( this IEnumerable<T> enumerable ) {
            List<T> buffer = new List<T>( enumerable );
            for ( int i = 0; i < buffer.Count; i++ ) {
                int index = random.Next( i, buffer.Count );
                yield return buffer[ index ];

                buffer[ index ] = buffer[ i ];
            }
        }

        public static void Shuffle<T>( this IList<T> list ) {
            int currentIndex = list.Count;
            while ( currentIndex > 1 ) {
                currentIndex--;
                int randomIndex = random.Next( currentIndex + 1 );

                // Swap
                T temp = list[ randomIndex ];
                list[ randomIndex ] = list[ currentIndex ];
                list[ currentIndex ] = temp;
            }
        }
    }
}
