﻿using UnityEngine;

namespace DeltaPosition {
    public interface ICursor {
        bool GetButton();
        bool GetButtonDown();
        bool GetButtonUp();
        Vector2 GetCursorPosition();
    }
}
