﻿using System.Collections;
using UnityEngine.SceneManagement;

namespace DeltaPosition {
    public class IntegrationTest {
        private Scene tempScene;

        protected IEnumerator LoadTestScene() {
            tempScene = SceneManager.GetActiveScene();
            string sceneName = GetType().Name;
            yield return SceneManager.LoadSceneAsync( sceneName, LoadSceneMode.Additive );
            SceneManager.SetActiveScene( SceneManager.GetSceneByName( sceneName ) );
        }

        protected IEnumerator UnloadTestScene() {
            SceneManager.SetActiveScene( tempScene );
            yield return SceneManager.UnloadSceneAsync( GetType().Name );
        }
    }
}
