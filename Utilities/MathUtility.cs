﻿namespace DeltaPosition {
    public static class MathUtility {
        public static int RoundToInt( float f ) {
            int floorValue = (int)f;
            if ( f >= 0f ) {
                if ( f - floorValue >= 0.5f ) {
                    return floorValue + 1;
                }
                else {
                    return floorValue;
                }
            }
            else {
                if ( floorValue - f >= 0.5f ) {
                    return floorValue - 1;
                }
                else {
                    return floorValue;
                }
            }
        }
    }
}
