﻿using System.Collections;

namespace DeltaPosition {
    public class StateTransition<T> where T : class {
        public MonoState<T> NextState { get; private set; }
        public ITransitionMethod<T> TransitionMethod { get; private set; }

        public StateTransition( MonoState<T> nextState, ITransitionMethod<T> transitionMethod = null ) {
            NextState = nextState;
            TransitionMethod = transitionMethod;
        }
    }

    public interface ITransitionMethod<T> {
        IEnumerator BeforeGoToState( T instance );
        IEnumerator AfterGoToState( T instance );
    }
}
