﻿namespace DeltaPosition {
    public abstract class State<T> where T : class {
        public virtual void Enter( T instance ) {

        }

        public virtual State<T> Update( T instance ) {
            return null;
        }

        public virtual void Exit( T instance ) {

        }
    }
}
